package com.twilio.streaming;

/*
 * Copyright 2012-2016 Amazon.com, Inc. or its affiliates. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License.
 * A copy of the License is located at
 *
 *  http://aws.amazon.com/apache2.0
 *
 * or in the "license" file accompanying this file. This file is distributed
 * on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied. See the License for the specific language governing
 * permissions and limitations under the License.
 */

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.nio.ByteBuffer;
import java.util.UUID;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.amazonaws.AmazonClientException;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.auth.profile.ProfileCredentialsProvider;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClient;
import com.amazonaws.services.ec2.AmazonEC2Client;
import com.amazonaws.services.kinesis.AmazonKinesis;
import com.amazonaws.services.kinesis.AmazonKinesisClient;
import com.amazonaws.services.kinesis.clientlibrary.interfaces.IRecordProcessorFactory;
import com.amazonaws.services.kinesis.clientlibrary.lib.worker.InitialPositionInStream;
import com.amazonaws.services.kinesis.clientlibrary.lib.worker.KinesisClientLibConfiguration;
import com.amazonaws.services.kinesis.clientlibrary.lib.worker.Worker;
import com.amazonaws.services.kinesis.model.DescribeStreamResult;
import com.amazonaws.services.kinesis.model.PutRecordRequest;
import com.amazonaws.services.kinesis.model.ResourceNotFoundException;

/**
 * Sample Amazon Kinesis Application.
 */
public final class AmazonKinesisMain {
	private static final Log LOG = LogFactory.getLog(AmazonKinesisMain.class);

	/*
	 * Before running the code: Fill in your AWS access credentials in the
	 * provided credentials file template, and be sure to move the file to the
	 * default location (/Users/ktoraskar/.aws/credentials) where the sample
	 * code will load the credentials from.
	 * https://console.aws.amazon.com/iam/home?#security_credential
	 * 
	 * WARNING: To avoid accidental leakage of your credentials, DO NOT keep the
	 * credentials file in your source directory.
	 */

	public static final String SAMPLE_APPLICATION_STREAM_NAME = "twilio-calls-stream";

	private static final String SAMPLE_APPLICATION_NAME = "SampleKinesisApplication";

	// Initial position in the stream when the application starts up for the
	// first time.
	// Position can be one of LATEST (most recent data) or TRIM_HORIZON (oldest
	// available data)
	private static final InitialPositionInStream SAMPLE_APPLICATION_INITIAL_POSITION_IN_STREAM = InitialPositionInStream.LATEST;

	private static AWSCredentialsProvider credentialsProvider;
	private static AWSCredentials awsCreds;

	private static void init() {
		// Ensure the JVM will refresh the cached IP values of AWS resources
		// (e.g. service endpoints).
		// java.security.Security.setProperty("networkaddress.cache.ttl", "60");

		/*
		 * The ProfileCredentialsProvider will return your [eclipseuser]
		 * credential profile by reading from the credentials file located at
		 * (/Users/ktoraskar/.aws/credentials).
		 */
		String profileName = "eclipseuser";
		//credentialsProvider = new ProfileCredentialsProvider(profileName);
		
		AmazonEC2Client client =
			    new AmazonEC2Client(new ProfileCredentialsProvider(profileName));
		
//		try {
//			credentialsProvider.getCredentials();
//		} catch (Exception e) {
//			throw new AmazonClientException(
//					"Cannot load the credentials from the credential profiles file. "
//							+ "Please make sure that your credentials file is at the correct "
//							+ "location (/Users/ktoraskar/.aws/credentials), and is in valid format.",
//					e);
//		}
	}

	public static void kinesisStreamMain(byte[] bytes, String partitionName)
			throws UnknownHostException {
		//init();

		String profileName = "eclipseuser";
		//credentialsProvider = new ProfileCredentialsProvider(profileName);
		
		awsCreds = new BasicAWSCredentials("AKIAJSWH2RTQFKB6J2MQ", "4TgBK0Nz3yuirSL08WjPywd1hopxL0D5cqoC0jF6");
		
		AmazonEC2Client client =
			    new AmazonEC2Client(awsCreds);
		
		
		String workerId = InetAddress.getLocalHost().getCanonicalHostName()
				+ ":" + UUID.randomUUID();
		KinesisClientLibConfiguration kinesisClientLibConfiguration = new KinesisClientLibConfiguration(
				SAMPLE_APPLICATION_NAME, SAMPLE_APPLICATION_STREAM_NAME,
				credentialsProvider, workerId);
		kinesisClientLibConfiguration
				.withInitialPositionInStream(SAMPLE_APPLICATION_INITIAL_POSITION_IN_STREAM);

		IRecordProcessorFactory recordProcessorFactory = new AmazonKinesisApplicationRecordProcessorFactory();
		Worker worker = new Worker(recordProcessorFactory,
				kinesisClientLibConfiguration);

		System.out.printf("Running %s to process stream %s as worker %s...\n",
				SAMPLE_APPLICATION_NAME, SAMPLE_APPLICATION_STREAM_NAME,
				workerId);

		AWSCredentials credentials = awsCreds;//credentialsProvider.getCredentials();
		AmazonKinesis kinesisClient = new AmazonKinesisClient(credentials);
		// Validate that the stream exists and is active
		validateStream(kinesisClient, SAMPLE_APPLICATION_STREAM_NAME);

		// Repeatedly send stock trades with a 100 milliseconds wait in between
		while (true) {
			sendDataToStream(bytes, kinesisClient,
					SAMPLE_APPLICATION_STREAM_NAME, partitionName);
			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

	}

//	public static void deleteResources() {
//		AWSCredentials credentials = //credentialsProvider.getCredentials();
//
//		// Delete the stream
//		AmazonKinesis kinesis = new AmazonKinesisClient(credentials);
//		System.out
//				.printf("Deleting the Amazon Kinesis stream used by the sample. Stream Name = %s.\n",
//						SAMPLE_APPLICATION_STREAM_NAME);
//		try {
//			kinesis.deleteStream(SAMPLE_APPLICATION_STREAM_NAME);
//		} catch (ResourceNotFoundException ex) {
//			// The stream doesn't exist.
//		}
//
//		// Delete the table
//		AmazonDynamoDBClient dynamoDB = new AmazonDynamoDBClient(
//				credentialsProvider.getCredentials());
//		System.out
//				.printf("Deleting the Amazon DynamoDB table used by the Amazon Kinesis Client Library. Table Name = %s.\n",
//						SAMPLE_APPLICATION_NAME);
//		try {
//			dynamoDB.deleteTable(SAMPLE_APPLICATION_NAME);
//		} catch (com.amazonaws.services.dynamodbv2.model.ResourceNotFoundException ex) {
//			// The table doesn't exist.
//		}
//	}

	private static void validateStream(AmazonKinesis kinesisClient,
			String streamName) {
		try {
			DescribeStreamResult result = kinesisClient
					.describeStream(streamName);
			if (!"ACTIVE".equals(result.getStreamDescription()
					.getStreamStatus())) {
				System.err
						.println("Stream "
								+ streamName
								+ " is not active. Please wait a few moments and try again.");
				System.exit(1);
			}
		} catch (ResourceNotFoundException e) {
			System.err.println("Stream " + streamName
					+ " does not exist. Please create it in the console.");
			System.err.println(e);
			System.exit(1);
		} catch (Exception e) {
			System.err.println("Error found while describing the stream "
					+ streamName);
			System.err.println(e);
			System.exit(1);
		}
	}

	private static void sendDataToStream(byte[] data,
			AmazonKinesis kinesisClient, String streamName, String partitionKey) {
		// TODO: Implement method

		byte[] bytes = data;
		// The bytes could be null if there is an issue with the JSON
		// serialization by the Jackson JSON library.
		if (bytes == null) {
			LOG.warn("Could not get JSON bytes for stock trade");
			return;
		}

		LOG.info("Putting a data record: ");
		PutRecordRequest putRecord = new PutRecordRequest();
		putRecord.setStreamName(streamName);
		putRecord.setPartitionKey(partitionKey);
		putRecord.setData(ByteBuffer.wrap(bytes));

		try {
			kinesisClient.putRecord(putRecord);
		} catch (AmazonClientException ex) {
			LOG.warn("Error sending record to Amazon Kinesis.", ex);
		}
	}

}

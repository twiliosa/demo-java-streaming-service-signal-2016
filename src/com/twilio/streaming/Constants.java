package com.twilio.streaming;

public class Constants {
	
	public static String LEAD = "Lead";
	public static String PHONENUMBER = "PhoneNumber";
	public static String CALLS = "Calls";
	public static String EVENT_CALLMAPPING = "CallMapping";

}

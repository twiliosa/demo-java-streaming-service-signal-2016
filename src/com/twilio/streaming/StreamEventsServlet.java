package com.twilio.streaming;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.UnknownHostException;
import java.util.logging.Logger;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.amazonaws.util.Base64;

public class StreamEventsServlet extends HttpServlet {

	private static final Logger logger = Logger
			.getLogger(StreamEventsServlet.class.getCanonicalName());

	public void service(HttpServletRequest request, HttpServletResponse response)
			throws IOException {

		String eventType = request.getParameter("requestType");
		String jsonObjectBase64 = request.getParameter("jsonObject");

		byte[] jsonObjectBytes = Base64.decode(jsonObjectBase64);

		if (eventType != null) {

			pushObjectToStream(eventType, jsonObjectBytes);
		}
		
		 response.setContentType("text/html");
		    PrintWriter out = response.getWriter();

		    out.println("<html>");
		    out.println("<head>");
		    out.println("<title>Hola</title>");
		    out.println("</head>");
		    out.println("<body bgcolor=\"white\">");
		    out.println("</body>");
		    out.println("</html>");

	}

	public void pushObjectToStream(String eventType, byte[] jsonObject)
			throws UnknownHostException {

		AmazonKinesisMain.kinesisStreamMain(jsonObject, eventType);

	}

}
